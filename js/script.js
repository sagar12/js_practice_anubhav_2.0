//Writing the code in the plugin form
(function( $ ){
	var methods = {
		moveLabel : function(field) {
			if(typeof(field) != 'undefined' && field.length > 0) {
				var fieldvalue = field.val();
				var length = fieldvalue.length;
				if(length > 0) {
					//Move label to the top
					field.prev().addClass("floated");
				} else {
					//Bring label to the original position
					field.prev().removeClass("floated");
				}
			}
		},
		validateCCField : function(field) {
			var ccfieldvalue = field.val();
			var ccfieldvaluelength = ccfieldvalue.length;
			if(ccfieldvaluelength <= 19) {
				//get only alphanumeric charaters
				var ccfieldvaluealphanumeric = ccfieldvalue.match(/\w|\d/g);
				var ccfieldwithspaces = methods.addSpacesInCCField(ccfieldvaluealphanumeric);
				field.val(ccfieldwithspaces);				
			} else {
				//limit length of credit card field to only 19 characters i.e. 16 characters and 3 spaces
				var ccfieldlimitcharacters = ccfieldvalue.substr(0, 19);
				field.val(ccfieldlimitcharacters);				
			}
		},
		addSpacesInCCField : function(arrayOfChars) {
			if(arrayOfChars.length > 0) {
				var arr = [];
				$.each(arrayOfChars, function(index, value) {
					if(index != 0 && index % 4 == 0) {
						arr.push(" " + value);
					} else {
						arr.push(value);
					}
				});
				return arr.join("");
			}
		}
	};

	$.fn.intelligentPlaceholder = function() {
		this.find("input.field").each(function(){
			var field = $(this);
			methods.moveLabel(field);
			field.keyup(function() {
				methods.moveLabel(field);
			});
		});
		return this;
	};

	$.fn.validateCreditCardField = function() {
		if(typeof(this) != 'undefined' && this.length > 0) {
			this.keyup(function() {
				methods.validateCCField($(this));
			});			
		}
	};
}( jQuery ));

// your code goes below
$(document).ready(function(){
	$("form").intelligentPlaceholder();
	$("#credit-card").validateCreditCardField();
});